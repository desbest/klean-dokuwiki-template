# Klean dokuwiki template

* Based on a wordpress theme
* Designed by [Inkhive](https://wordpress.org/themes/klean/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:klean)

![klean theme screenshot](https://i.imgur.com/UtRj1Wd.png)