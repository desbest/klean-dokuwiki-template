<?php
/**
 * DokuWiki Klean Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:klean
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <script defer type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <link rel='stylesheet' id='klean-title-font-css'  href='//fonts.googleapis.com/css?family=Source+Sans+Pro%3A100%2C300%2C400%2C700&#038;ver=5.4.4' type='text/css' media='all' />
        <link rel='stylesheet' id='klean-body-font-css'  href='//fonts.googleapis.com/css?family=Source+Sans+Pro%3A100%2C300%2C400%2C700&#038;ver=5.4.4' type='text/css' media='all' />      
</head>

<body id="dokuwiki__top"  class="site post-template-default single single-post postid-9 single-format-standard logged-in admin-bar  customize-support <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">

    <?php //get_template_part('modules/navigation/primary', 'menu'); ?>

    <nav id="site-navigation" class="main-navigation" role="navigation">
        <button class="menu-toggle" aria-controls="menu" aria-expanded="false">Primary Menu</button>
        <ul class="menu nav-menu" aria-expanded="false">
            <!-- SITE TOOLS -->
            <h3 class="a11y"><?php echo $lang['site_tools'] ?></h3>
                <?php tpl_toolsevent('sitetools', array(
                    'recent'    => tpl_action('recent', 1, 'li', 1),
                    'media'     => tpl_action('media', 1, 'li', 1),
                    'index'     => tpl_action('index', 1, 'li', 1),
                )); ?>
                 <?php if ($showTools): ?>
                <div class="mobile">
                <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                    <?php tpl_toolsevent('pagetools', array(
                    'edit'      => tpl_action('edit', 1, 'li', 1),
                    'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                    'revisions' => tpl_action('revisions', 1, 'li', 1),
                    'backlink'  => tpl_action('backlink', 1, 'li', 1),
                    'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                    'revert'    => tpl_action('revert', 1, 'li', 1),
                    //'top'       => tpl_action('top', 1, 'li', 1),
                )); ?>
                </div>
                <?php endif; ?>
                <!-- USER TOOLS -->
                <?php if ($conf['useacl'] && $showTools): ?>
                <div class="mobile">
                <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
                    <?php
                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">'; tpl_userinfo(); /* 'Logged in as ...' */ echo '</li>';
                        }
                    ?>
                    <?php tpl_toolsevent('usertools', array(
                            'admin'     => tpl_action('admin', 1, 'li', 1),
                            'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                            'profile'   => tpl_action('profile', 1, 'li', 1),
                            'register'  => tpl_action('register', 1, 'li', 1),
                            'login'     => tpl_action('login', 1, 'li', 1),
                        )); ?>
                    </div>
                    <?php endif; ?>
                    <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                             e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
                    ?>
    </ul>
    </nav>

    <?php tpl_includeFile('header.html') ?>
    <div id="container">
    <div id="header-wrapper">
        <header id="masthead" class="site-header" role="banner">

            <ul class="a11y skip">
                <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
            </ul>

            <div class="header-image">
            </div><!-- .header-image -->

            <div class="site-branding container">
            <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                        tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
            <div id="text-title-desc">
                <h1 class="site-title"><a href="" rel="home"><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></a></h1>
                <?php if ($conf['tagline']): ?><h2 class="site-description"><?php echo $conf['tagline'] ?></h2><?php endif; ?>
            </div>


            <div id="secondary" class="widget-area col-lg-3 desktop" role="complementary"><!-- .left-sidebar or .right-sidebar for #secondary -->

            <aside id="writtensidebar" class="widget">
            <!-- <h1 class="widget-title">Page Tools</h1> -->

            <!-- ********** ASIDE ********** -->
            <?php if ($showSidebar): ?>
                <div id="dokuwiki__aside">
                    <?php tpl_includeFile('sidebarheader.html') ?>
                    <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                    <?php tpl_includeFile('sidebarfooter.html') ?>
                    <div class="clearer"></div>
                </div><!-- /aside -->
            <?php endif; ?>
            </aside>


            <aside id="pages-3" class="widget"><h1 class="widget-title">Page Tools</h1><ul>
                 <!-- PAGE ACTIONS -->
            <?php if ($showTools): ?>
                <!-- <div id="dokuwiki__pagetools"> -->
                <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                    <?php tpl_toolsevent('pagetools', array(
                    'edit'      => tpl_action('edit', 1, 'li', 1),
                    'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                    'revisions' => tpl_action('revisions', 1, 'li', 1),
                    'backlink'  => tpl_action('backlink', 1, 'li', 1),
                    'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                    'revert'    => tpl_action('revert', 1, 'li', 1),
                    'top'       => tpl_action('top', 1, 'li', 1),
                )); ?>
            <?php endif; ?>
            </ul></aside></div>

            <div id="secondary" class="widget-area col-lg-3 desktop" role="complementary"><!-- .left-sidebar or .right-sidebar for #secondary --><aside id="pages-3" class="widget"><h1 class="widget-title">User Tools</h1><ul>
                 <!-- USER TOOLS -->
                 <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
                    <?php
                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">'; tpl_userinfo(); /* 'Logged in as ...' */ echo '</li>';
                        }
                    ?>
            <?php if ($showTools): ?>
                <!-- <div id="dokuwiki__pagetools"> -->
                <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                 <?php tpl_toolsevent('usertools', array(
                        'admin'     => tpl_action('admin', 1, 'li', 1),
                        'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                        'profile'   => tpl_action('profile', 1, 'li', 1),
                        'register'  => tpl_action('register', 1, 'li', 1),
                        'login'     => tpl_action('login', 1, 'li', 1),
                    )); ?>
            <?php endif; ?>
            </ul></aside></div>

       

            </div><!-- .site-branding -->
        </header><!-- #masthead -->

    

        <div id="search-top">
            <?php tpl_searchform() ?>
            <!-- <form method="get" id="searchform" action="/">
                <div><input type="text" size="18" value="" name="s" id="s" />
                    <button type="submit" class="search-submit">
                        <img src = "<?php echo tpl_basedir() . '/images/search.png'?>" width="17px" height="17px">
                    </button>
                </div>
            </form> -->
        </div>
        <?php //get_template_part('modules/social/social', 'fa'); ?>
    </div>

<div id="page" class="hfeed site container">
        <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

    <div id="content" class="site-content container">
        <!-- content begins -->
        <div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <main id="main" class="site-main" role="main">

            <?php 
            //get_template_part( 'modules/content/content', 'single' );
            ?>

            <article>
                <!-- <header class="entry-header">
                <h1 class="entry-title"><a href="urlhere" rel="bookmark">post title</a></h1>
                <div class="entry-meta">
                    <span class="posted-on"><a href="http://localhost/wordpress/test-post/" rel="bookmark"><time class="updated published" datetime="2020-05-31T16:53:41+00:00">May 31, 2020</time></a></span>
                </div>
                </header> -->

                <div class="entry-content">

                     <!-- BREADCRUMBS -->
                    <?php if($conf['breadcrumbs']){ ?>
                        <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
                    <?php } ?>
                    <?php if($conf['youarehere']){ ?>
                        <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
                    <?php } ?>

                    <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>

                    <!-- ********** CONTENT ********** -->
                    <div id="dokuwiki__content">
                        <?php tpl_flush() /* flush the output buffer */ ?>
                        <?php tpl_includeFile('pageheader.html') ?>

                        <div class="page">
                        <!-- wikipage start -->
                        <?php tpl_content() /* the main content */ ?>
                        <!-- wikipage stop -->
                        <div class="clearer"></div>
                        </div>

                        <?php tpl_flush() ?>
                        <?php tpl_includeFile('pagefooter.html') ?>
                    </div><!-- /content -->
                

                
                </div><!-- .entry-content -->

                <footer class="entry-footer">
                
                </footer><!-- .entry-footer -->
            </article><!-- #post-## -->

            <?php
            // If comments are open or we have at least one comment, load up the comment template
            // if ( comments_open() || get_comments_number() ) :
            // comments_template();
            // endif;
            ?>

            <aside id="writtensidebar" class="widget mobile">
            <!-- <h1 class="widget-title">Page Tools</h1> -->

            <!-- ********** ASIDE ********** -->
            <?php if ($showSidebar): ?>
                <div id="dokuwiki__aside">
                    <?php tpl_includeFile('sidebarheader.html') ?>
                    <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                    <?php tpl_includeFile('sidebarfooter.html') ?>
                    <div class="clearer"></div>
                </div><!-- /aside -->
            <?php endif; ?>
            </aside>

        </main><!-- #main -->
        </div><!-- #primary -->
        <!-- content ends -->
        </div><!-- #content -->

        <!-- sidebar begins -->

        <!-- sidebar ends -->

    <footer id="colophon" class="site-footer" role="contentinfo">
    <div id="footer-sidebar" class="widget-area clear container" role="complementary">
        <div class="footer-column col-lg-4 col-md-4 col-sm-12 col-xs-12"> 
            <?php //dynamic_sidebar( 'sidebar-4'); ?> 
        </div>
            
        <div class="footer-column col-lg-4 col-md-4 col-sm-12 col-xs-12"> 
            <?php //dynamic_sidebar( 'sidebar-5'); ?> 
        </div>
    </div>
        <div class="site-info">
            <div class="footer-text">
                <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
                <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>

                &copy; <?php echo date("Y"); ?> <?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?>. All Rights Reserved.
            </div>
            <div class="powered-by">
                <a href="http://dokuwiki.org">Powered by Dokuwiki</a>
                <span class="sep"> | </span>
                Klean theme by <a href="https://www.inkhive.com" rel="designer">InkHive</a> and <a href="http://desbest.com">desbest</a>

                <?php tpl_includeFile('footer.html') ?>
            </div>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->
</div><!-- #page -->
</div><!-- #container -->

    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>
        


          


    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>

    <!-- slicknav is for the responsive menu on mobile -->
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/jquery.slicknav.min.js"></script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/runslicknav.js"></script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/navigation.js"></script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/mobileinputsize.js"></script>
    <!-- due to the way dokuwiki buffers output, this javascript has to
            be before the </body> tag and not in the <head> -->
</body>
</html>
